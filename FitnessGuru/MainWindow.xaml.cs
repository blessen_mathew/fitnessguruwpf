﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FitnessGuru
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Data that will be used to populate fields
        string[] fieldNames = new string[] {"FirstName", "LastName", "DOB" ,"Address", "City", "Province", "Postal", "Comments", "Trainer", "Program", "Bank", "Timing" };
        string[] months = new string[] { (string)Properties.Resources.Jan, (string)Properties.Resources.Feb, (string)Properties.Resources.Mar, (string)Properties.Resources.Apr, (string)Properties.Resources.May, (string)Properties.Resources.Jun, (string)Properties.Resources.Jul, (string)Properties.Resources.Aug, (string)Properties.Resources.Sep, (string)Properties.Resources.Oct, (string)Properties.Resources.Nov, (string)Properties.Resources.Dec };
        string[] provinces = new string[] { "AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT" };
        string[] trainers = new string[] { "Mike Tyson", "Harvey Specter", "Clark Kent", "Michael Jordan", "Keanu Reeves", "Mick Jagger", "Jim Morrison", "Van Halen", "Alice Cooper", "Deep Purple" };

        //Array that will hold data when saving to file
        string[] fieldValues = new string[12];

        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;  //sets the window startup location to be centered
        }

        //Populates any comboboxes with defined data needed prior to any user input
        private void PopulateFields(object sender, RoutedEventArgs e)
        {
            //populate the year combobox up to the current year
            for (int i = 1910; i <= DateTime.Now.Year; i++)
            {
                yearComboBox.Items.Add(i);
            }
            yearComboBox.SelectedValue = yearComboBox.Items.GetItemAt(yearComboBox.Items.Count - 1);

            //populate month combobox
            monthComboBox.DisplayMemberPath = "Key";
            monthComboBox.SelectedValuePath = "Value";
            for (int i = 1; i < months.Length + 1; i++)
            {
                monthComboBox.Items.Add(new KeyValuePair<string, int>(months[i - 1], i));
            }

            //populate provinces combobox
            provComboBox.ItemsSource = provinces;

            //populate trainers combobox
            trainerComboBox.ItemsSource = trainers;
        }

        //Called when the textbox is brought into focus 
        //if the placeholder text is there, it will be removed so user can begin their input.
        private void PlaceHolder(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text == (string)Properties.Resources.TextPlaceholder)
            {
                tb.Text = "";
                tb.Foreground = (Brush)FindResource("RealText");
            }
        }

        //Called when the textbox is out of focus
        //If the user typed nothing, placeholder text is placed in the textbox
        private void PlaceHolderAdd(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text == "")
            {
                tb.Text = (string)Properties.Resources.TextPlaceholder;
                tb.Foreground = (Brush)FindResource("GreyText");
            }
        }

        //When the year and month combobox are changed the day combobox is updated with the correct number of days
        private void UpdateDays(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            if (cbo.Name == "monthComboBox")
                borderMonthCbo.BorderBrush = (Brush)FindResource("DefaultBorder");      

            //only calculate days if a selection from the month combobox is selected
            if (Convert.ToInt32(monthComboBox.SelectedIndex) != -1)
            {
                setDayCombobox();
            }
        }

        //Calculates day based on year and month, populates day combobox and enables it
        public void setDayCombobox()
        {
            int year = (int)yearComboBox.SelectedItem;
            int month = (int)((KeyValuePair<string, int>)monthComboBox.SelectedItem).Value;
            int days = DateTime.DaysInMonth(year, month);   //calculates the number of days for the month in selected year

            dayComboBox.Items.Clear();  //clears the day combobox

            for (int i = 1; i <= days; i++)
            {
                dayComboBox.Items.Add(i);
            }
            dayComboBox.IsEnabled = true;   //enables the day combobox
        }

        //Removes any error styling from a combobox
        private void removeBorderErrorCBO(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cbo = sender as ComboBox;
            Border brd = (Border)cbo.Parent;
            brd.BorderBrush = (Brush)FindResource("DefaultBorder");
        }

        //When the user wants to submit the form
        private void SubmitData(object sender, RoutedEventArgs e)
        {
            if(checkAndValidateData() == true)
            {
                WriteToFile();
            }
        }

        //Writes data to a text file
        private void WriteToFile()
        {
            getAllData(); //Populates an array with data that the user input

            var sb = new StringBuilder();
            var filename = (string)Properties.Resources.Filename;
            for (int i = 0; i < fieldNames.Length; i++)
            {
                var first = fieldNames[i];
                var second = fieldValues[i];
                var newLine = string.Format("{0}:{1}", first, second);
                sb.AppendLine(newLine);
            }
            File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + filename, sb.ToString());
            MessageBox.Show("The data was saved successfully!");
            Application.Current.Shutdown();
        }

        //Checks if there is data in all required fields
        //Validates any fields that require specific form of data
        //Returns true if all data is correct and present, otherwise returns false
        public bool checkAndValidateData()
        {
            bool complete = true;

            if (firstNameTextBox.Text == (string)Properties.Resources.TextPlaceholder)
            {
                firstNameTextBox.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (lastNameTextBox.Text == (string)Properties.Resources.TextPlaceholder)
            {
                lastNameTextBox.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (validateNames(firstNameTextBox.Text, lastNameTextBox.Text) == false) //checks that the names are valid format
            {
                complete = false;
            }
            if (monthComboBox.SelectedIndex == -1)
            {
                borderMonthCbo.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (dayComboBox.SelectedIndex == -1)
            {
                borderDayCbo.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (addressTextBox.Text == (string)Properties.Resources.TextPlaceholder)
            {
                addressTextBox.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (cityTextBox.Text == (string)Properties.Resources.TextPlaceholder)
            {
                cityTextBox.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (provComboBox.SelectedIndex == -1)
            {
                borderProvCbo.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (postalTextBox.Text == (string)Properties.Resources.TextPlaceholder)
            {
                postalTextBox.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (validatePostalCode() == false)  //checks if the postal code is in a correct format
            {
                complete = false;
            } 
            if (trainerComboBox.SelectedIndex == -1)
            {
                borderTrainerCbo.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            if (bankingTimePeriod() == false)   //need to select 1 banking timing option
            {
                monthlyRadioButton.BorderBrush = (Brush)FindResource("ErrorBorder");
                annualRadioButton.BorderBrush = (Brush)FindResource("ErrorBorder");
                complete = false;
            }
            return complete;
        }

        //Returns true if the user selected one of the banking timing options
        public bool bankingTimePeriod()
        {
            if (annualRadioButton.IsChecked == true || monthlyRadioButton.IsChecked == true)
                return true;
            else
                return false;
        }

        //checks if the postal code is in a valid format.
        //Adds error styling to textbox if not valid
        public bool validatePostalCode()
        {
            if (Regex.IsMatch(postalTextBox.Text, @"^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$") == false)
            {
                postalTextBox.BorderBrush = (Brush)FindResource("ErrorBorder");
                return false;
            }
            else
                return true;
        }

        //Checks if the names entered only contain letters
        //Adds error styling to the textboxes if not valid
        public bool validateNames(string fname, string lname)
        {
            bool validName = true;
            if(Regex.IsMatch(fname, @"(?i)^[a-z]+") == false)
            {
                firstNameTextBox.BorderBrush = (Brush)FindResource("ErrorBorder");
                validName = false;
            }
            if (Regex.IsMatch(lname, @"(?i)^[a-z]+") == false)
            {
                lastNameTextBox.BorderBrush = (Brush)FindResource("ErrorBorder");
                validName = false;
            }
            return validName;
        }

        //Populates array of values from what user input
        //Array is used when writing data to file
        private void getAllData()
        {
            fieldValues[0] = firstNameTextBox.Text;
            fieldValues[1] = lastNameTextBox.Text;
            fieldValues[2] = yearComboBox.SelectedItem.ToString() + "/" + ((KeyValuePair<string,int>)monthComboBox.SelectedItem).Value.ToString() + "/" + dayComboBox.SelectedItem.ToString();
            fieldValues[3] = addressTextBox.Text;
            fieldValues[4] = cityTextBox.Text;
            fieldValues[5] = provComboBox.SelectedItem.ToString();
            fieldValues[6] = postalTextBox.Text;
            fieldValues[7] = commentTextBox.Text;
            fieldValues[8] = trainerComboBox.SelectedItem.ToString();

            if (beginnerRadioButton.IsChecked == true)
                fieldValues[9] = (string)Properties.Resources.ProgramLevel1;
            else if (experiencedRadioButton.IsChecked == true)
                fieldValues[9] = (string)Properties.Resources.ProgramLevel2;
            else
                fieldValues[9] = (string)Properties.Resources.ProgramLevel3;

            if (bankAccountCheckBox.IsChecked == true)
                fieldValues[10] = (string)Properties.Resources.BillingInformation1;
            else
                fieldValues[10] = (string)Properties.Resources.NotAvailable;

            if (monthlyRadioButton.IsChecked == true)
                fieldValues[11] = (string)Properties.Resources.BillingInformation2;
            if (annualRadioButton.IsChecked == true)
                fieldValues[11] = (string)Properties.Resources.BillingInformation3;
        }

        //loads data from files if it exists
        private void LoadFromFile(object sender, RoutedEventArgs e)
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + (string)Properties.Resources.Filename;
            if(!File.Exists(filePath))
            {
                MessageBox.Show("The file doesn't exist! You need to submit the form first!");
            }
            else
            {
                loadFormWithData(filePath);
                MessageBox.Show("Data was loaded!");
            }
        }

        //Loads the form with any previous data that exists in a saved file
        public void loadFormWithData(string filePath)
        {
            string[] data = File.ReadAllLines(filePath);
            for(int i = 0; i < data.Length;i++)
            {
                string line = data[i];
                int split = line.IndexOf(":");
                string field = line.Substring(0, split);
                string value = line.Substring(split+1);

                switch(field)
                {
                    case "FirstName":
                        firstNameTextBox.Text = value;
                        firstNameTextBox.Foreground = (Brush)FindResource("RealText");
                        fieldValues[0] = value;
                        break;
                    case "LastName":
                        lastNameTextBox.Text = value;
                        lastNameTextBox.Foreground = (Brush)FindResource("RealText");
                        fieldValues[1] = value;
                        break;
                    case "DOB":
                        int firstSplit = value.IndexOf("/");
                        int secondSplit = value.LastIndexOf("/");
                        string year = value.Substring(0, firstSplit);
                        string month = value.Substring(firstSplit+1, (secondSplit-firstSplit)-1);
                        string day = value.Substring(secondSplit+1);

                        yearComboBox.SelectedValue = Int32.Parse(year);
                        monthComboBox.SelectedValue = Int32.Parse(month);
                        dayComboBox.SelectedValue = Int32.Parse(day);
                        fieldValues[2] = value;
                        break;
                    case "Address":
                        addressTextBox.Text = value;
                        addressTextBox.Foreground = (Brush)FindResource("RealText");
                        fieldValues[3] = value;
                        break;
                    case "City":
                        cityTextBox.Text = value;
                        cityTextBox.Foreground = (Brush)FindResource("RealText");
                        fieldValues[4] = value;
                        break;
                    case "Province":
                        provComboBox.SelectedValue = value;
                        fieldValues[5] = value;
                        break;
                    case "Postal":
                        postalTextBox.Text = value;
                        postalTextBox.Foreground = (Brush)FindResource("RealText");
                        fieldValues[6] = value;
                        break;
                    case "Comments":
                        commentTextBox.Text = value;
                        fieldValues[7] = value;
                        break;
                    case "Trainer":
                        trainerComboBox.SelectedValue = value;
                        fieldValues[8] = value;
                        break;
                    case "Program":
                        if (value == Properties.Resources.ProgramLevel1)
                            beginnerRadioButton.IsChecked = true;
                        else if (value == Properties.Resources.ProgramLevel2)
                            experiencedRadioButton.IsChecked = true;
                        else
                            highPerformanceRadioButton.IsChecked = true;
                        fieldValues[9] = value;
                        break;
                    case "Bank":
                        if (value == Properties.Resources.BillingInformation1)
                            bankAccountCheckBox.IsChecked = true;
                        else
                            bankAccountCheckBox.IsChecked = false;
                        fieldValues[10] = value;
                        break;
                    case "Timing":
                        if (value == Properties.Resources.BillingInformation2)
                            monthlyRadioButton.IsChecked = true;
                        else
                            annualRadioButton.IsChecked = true;
                        fieldValues[11] = value;
                        break;
                    default:
                        break;
                }
            }
            //Make the foreground for the textboxes RealText
        }

        //Calls close app function
        private void CloseAppEvent(object sender, RoutedEventArgs e)
        {
            closeApp();
        }

        //Closes the application
        //Will prompt user if there is any unsaved data in form
        private void closeApp()
        {
            //if there is unsaved data, ask the user if they really want to exit the app and lose the current data.
            if (checkUnsavedData() == true)
            {
                MessageBoxResult result;
                //if there is data in the form, check if the data is the same data that was loaded
                //if the loaded data differs from what is in the form, it is different and the user is warned
                //otherwise the application closes
                if (alteredLoadedData() == true)
                {
                    result = MessageBox.Show("You have unsaved data! Exiting the program will lose data. Are you sure you want to exit?", "Warning", MessageBoxButton.YesNo);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            Application.Current.Shutdown();
                            break;
                        case MessageBoxResult.No:
                            break;
                    }
                }
                else
                    Application.Current.Shutdown();
            }
            else
            {
                //If no unsaved data, close the application
                Application.Current.Shutdown();
            }
        }

        //Checks if there is any data that the user has not saved yet
        public bool checkUnsavedData()
        {
            bool unsaved = false;
            if (firstNameTextBox.Text != (string)Properties.Resources.TextPlaceholder || lastNameTextBox.Text != (string)Properties.Resources.TextPlaceholder)
                unsaved = true;
            if (monthComboBox.SelectedIndex != -1 || dayComboBox.SelectedIndex != -1 || provComboBox.SelectedIndex != -1 || trainerComboBox.SelectedIndex != -1)
                unsaved = true;
            if (addressTextBox.Text != (string)Properties.Resources.TextPlaceholder || cityTextBox.Text != (string)Properties.Resources.TextPlaceholder || postalTextBox.Text != (string)Properties.Resources.TextPlaceholder)
                unsaved = true;
            if (commentTextBox.Text != "")
                unsaved = true;
            if (bankAccountCheckBox.IsChecked == true)
                unsaved = true;
            if (monthlyRadioButton.IsChecked == true || annualRadioButton.IsChecked == true)
                unsaved = true;

            return unsaved;
        }

        //Checks if the data in the array is equal to the data that exists in the form.
        //Returns true if the data is altered in the form
        //Returns false if the data is unaltered.
        public bool alteredLoadedData()
        {
            bool newData = false;
            if (firstNameTextBox.Text != fieldValues[0])
                newData = true;
            if (lastNameTextBox.Text != fieldValues[1])
                newData = true;

            if (monthComboBox.SelectedIndex != -1 && dayComboBox.SelectedIndex != -1)
            {
                string dob = yearComboBox.SelectedItem.ToString() + "/" + ((KeyValuePair<string, int>)monthComboBox.SelectedItem).Value.ToString() + "/" + dayComboBox.SelectedItem.ToString();
                if (dob != fieldValues[2])
                    newData = true;
            }

            if (dayComboBox.SelectedIndex == -1)
                newData = true;

            if (addressTextBox.Text != fieldValues[3])
                newData = true;
            if (cityTextBox.Text != fieldValues[4])
                newData = true;

            if(provComboBox.SelectedIndex != -1)
            {
                if (provComboBox.SelectedItem.ToString() != fieldValues[5])
                    newData = true;
            }

            if (postalTextBox.Text != fieldValues[6])
                newData = true;
            if (commentTextBox.Text != fieldValues[7])
                newData = true;
 
            if(trainerComboBox.SelectedIndex != -1)
            {
                if (trainerComboBox.SelectedItem.ToString() != fieldValues[8])
                    newData = true;
            }

            if (beginnerRadioButton.IsChecked == true && fieldValues[9] != (string)Properties.Resources.ProgramLevel1)
                newData = true;
            if (experiencedRadioButton.IsChecked == true && fieldValues[9] != (string)Properties.Resources.ProgramLevel2)
                newData = true;
            if (highPerformanceRadioButton.IsChecked == true && fieldValues[9] != (string)Properties.Resources.ProgramLevel3)
                newData = true;

            if (bankAccountCheckBox.IsChecked == true && fieldValues[10] != (string)Properties.Resources.BillingInformation1)
                newData = true;

            if (monthlyRadioButton.IsChecked == true && fieldValues[11] != (string)Properties.Resources.BillingInformation2)
                newData = true;
            if (annualRadioButton.IsChecked == true && fieldValues[11] != (string)Properties.Resources.BillingInformation3)
                newData = true;

                return newData;
        }

        //Removes the error border on the control object and returns the default border style
        private void RemoveErrorBorder(object sender, TextChangedEventArgs e)
        {
            Control cntrl = (Control)sender;
            cntrl.BorderBrush = (Brush)FindResource("DefaultBorder");
        }

        //Removes the error border from raduibuttons and checkbox and returns the default border
        private void RemoveErrorBorder(object sender, RoutedEventArgs e)
        {
            Control cntrl = (Control)sender;
            if (cntrl.Name == "monthlyRadioButton" || cntrl.Name == "annualRadioButton")
            {
                monthlyRadioButton.BorderBrush = (Brush)FindResource("DefaultBorder");
                annualRadioButton.BorderBrush = (Brush)FindResource("DefaultBorder");
            }
            else
            {
                cntrl.BorderBrush = (Brush)FindResource("DefaultBorder");
            }
        }
    }
}
